import tornado.ioloop
import tornado.web
import boto3
import botocore
import paramiko,os
from datetime import datetime
import pytz

server_data=[]
file_name=[]
user_tar_temp=[]
#npath = os.path.dirname(os.path.realpath(__file__))
#os.path.join(npath, "/keys/"+server_data[2])

def awsConfigure(server_data):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        client.connect(server_data[0], 22, server_data[1], server_data[2])
        cmd = "pip install awscli --upgrade --user && aws configure --profile log_share"
        stdin, stdout, stderr = client.exec_command(cmd)

        client.close()
        return null
    except Exception as e:
        return e

def display_log(server_data):
    #host_name=server_data[0]
    #user=server_data[1]
    #npath = os.path.dirname(os.path.realpath(__file__))
    #temp_p="{}/{}".format("keys",server_data[2])
    #key_path =os.path.join(npath, temp_p)
    #key_path = server_data[2]
    path_in_server=server_data[3]
    #key = paramiko.RSAKey.from_private_key_file(key_path)
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        client.connect(server_data[0], 22, server_data[1], server_data[2])
        cmd = "ls -lshtrp "+path_in_server+" | grep -v / | awk '{print $10,$8,$7,$9,$1}'"
        stdin, stdout, stderr = client.exec_command(cmd)
        data = []
        #data=stdout.read().decode()
        while True:
          line = stdout.readline()
          if line != '':
            #the real code does filtering here
            data.append(line.rstrip())
          else:
            break
        del data[0]
        client.close()
        return data
    except Exception as e:
        return e

def send_file_name(file_list,file_name,user_tar_name_temp):
    #npath = os.path.dirname(os.path.realpath(__file__))
    #temp_p="{}/{}".format("keys",server_data[2])
    #key_path =os.path.join(npath, temp_p)
    #key = paramiko.RSAKey.from_private_key_file(key_path)
    ist=pytz.timezone('Asia/Calcutta')
    user_tar_name="{}-{}".format(user_tar_name_temp, datetime.now(ist).strftime("%Y%m%d%H%M%S"))
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        client.connect(server_data[0], 22, server_data[1], server_data[2])
        #cmd = "aws s3 cp /var/log/"+ file_name +" s3://wdlogdata"
        cmd = "cd "+server_data[3]+" && tar -cvf /tmp/"+user_tar_name+".tar "+file_name+" && aws s3 cp /tmp/"+user_tar_name+".tar"+" s3://wd-log-demo --acl public-read --profile log_share"
        stdin, stdout, stderr = client.exec_command(cmd)
        data=stdout.read().decode()
        cmd1="rm -f /tmp/"+user_tar_name+".tar"
        stdin1, stdout1, stderr1 = client.exec_command(cmd1)
        data1=stdout1.read().decode()
        std_out_put="{} {}".format(data,data1)
        client.close()
        #s3 = boto3.resource('s3')
        if data:
            info='Output: You selected: {} URL: {}/{}.tar'.format(file_list,"https://s3.amazonaws.com/wd-log-demo",user_tar_name)
        else:
            info='Error!!! You have selected the wrong log, which requires the root permission'
        return info
    except Exception as e:
        return e

class adminHandler(tornado.web.RequestHandler):
    def get(self, url='/admin'):
        self.render("admin.html")

    def post(self):
        del server_data[:]
        try:
            server_data_byte = self.request.arguments["file_name"]
            for aws_data in server_data_byte:
                server_data.append(aws_data.decode())
            awsConfigure(server_data)
            data=" {}".format(" Done ")
            self.render("output.html", entry=data)
        except Exception as e:
            data=" {}".format(" Unable to connect with the server!!!")
            self.render("Error.html", entry=e)

class loginHandler(tornado.web.RequestHandler):
    def get(self, url='/'):
        self.render("login.html")

class MainHandler(tornado.web.RequestHandler):
    def get(self, url='/main'):
        data=display_log(server_data)
        self.render("display.html", entry=data)

    def post(self):
        del server_data[:]
        try:
            server_data_byte = self.request.arguments["file_name"]
            for file in server_data_byte:
                server_data.append(file.decode())
            self.get(url='')
        except Exception as e:
            data=" {}".format(" Unable to connect with the server!!!")
            self.render("Error.html", entry=e)

class outputHandler(tornado.web.RequestHandler):
    def get(self,url='/output'):
        str_temp=""
        user_tar_name=""
        for i in file_name:
            str_temp=str_temp + i + " "

        data=send_file_name(file_name,str_temp,str(user_tar_temp[0]))
        #self.write("You Select " + self.get_body_argument("file_name"))
        #self.write("You Select " + user_tar_name)
        self.render("output.html", entry=data)

    def post(self):
        entry = None
        del file_name[:]
        del user_tar_temp[:]
        try:
            temp_file_name = self.request.arguments["file_selected"]
            #self.write("You Select " + self.get_body_argument("file_name"))
            user_tar_temp1 = self.request.arguments["file_name"]
            for file in user_tar_temp1:
                user_tar_temp.append(file.decode())

            for file in temp_file_name:
                file_name.append(file.decode())
            self.get(url='')
        except Exception as e:
            data=" {}".format(" Please select the data properly!!!")
            self.render("Error.html", entry=e)

def make_app():
    return tornado.web.Application([
        (r"/main", MainHandler),
        (r"/output", outputHandler),
        (r"/", loginHandler),
        (r"/sample", loginHandler),
        (r"/admin", adminHandler),
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8890)
    tornado.ioloop.IOLoop.current().start()
