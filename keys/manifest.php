<?php

//this is CMS mainfest file for more use
$manifest = array(
            'mysql.db' =>
                array(
                    'write' => array(
                        'host' => 'apiv18.c9pjzvztavlp.ap-southeast-1.rds.amazonaws.com',
                        'username' => 'apiv18',
                        'password' => 'apiv188d8w',
                        'dbname' => 'api',
                    ),
                    'read' => array(
                        'host' => 'apiv18-replica2.c9pjzvztavlp.ap-southeast-1.rds.amazonaws.com',
                        'username' => 'apiv18',
                        'password' => 'apiv188d8w',
                        'dbname' => 'api',
                    ),
                ),
            'shout.mysql.db' =>
                array(
                    'write' => array(
                        'host' => 'apiv18.c9pjzvztavlp.ap-southeast-1.rds.amazonaws.com',
                        'username' => 'apiv18',
                        'password' => 'apiv188d8w',
                        'dbname' => 'shout',
                    ),
                    'read' => array(
                        'host' => 'apiv18-replica2.c9pjzvztavlp.ap-southeast-1.rds.amazonaws.com',
                        'username' => 'apiv18',
                        'password' => 'apiv188d8w',
                        'dbname' => 'shout',
                    ),
                ),
            'redis' => array(
                'host' => 'voot-prod.mbzq1s.0001.apse1.cache.amazonaws.com',//'viacom-dev.mbzq1s.0001.apse1.cache.amazonaws.com'
                'port' => '6379',
                'isRedisOn' => true,
                'clearOldData' => TRUE
            ),
            "templateNameAndKey" => array(
                "MOVIES:LIST" => "MOVIES_LIST", //for movies home page listing and genre listing page Template
                "MOVIES:GENRE" => "MOVIES_LIST", //for movies home page listing and genre listing page Template
                "MOVIES:FULLMOVIE:DETAILS" => "MOVIES_DETAILS", //for movie and clips playback pages
                "MOVIES:CLIP:DETAILS" => "MOVIES_DETAILS", //for movie and clips playback pages
                
                "SHOWS:LIST" => "SHOWS_LIST", // for TV SERIES home page listing and genre listing page Template
                "SHOWS:GENRE" => "SHOWS_LIST", // for TV SERIES home page listing and genre listing page Template
                "SHOWS:DETAILS" => "SHOWS_DETAILS", //for TV SERIES details pages
                "SHOWS:SLUG:SEASON" => "SHOWS_SLUG_SEASON", // for TV SERIES listing page for all TV SERIES
                
                "SHOWS:FULLEPISODE:DETAILS" => "SHOWS_FULLEPISODE_DETAILS", //for playback page of Episodes of a TV Series 
                "SHOWS:CLIP:DETAILS" => "SHOWS_CLIP_DETAILS", //for playback page of clips of a TV Series 
            ),
            "pageType" => array(
                "SHOWS_LIST" => 1,
                "SHOWS_GENRE" => 10,
                "SHOWS_SLUG_SEASON" => 2,
                "SHOWS_DETAILS" => 3,

                "MOVIES_LIST" => 4,
                "MOVIES_GENRE" => 10,
                "MOVIES_FULLMOVIE_DETAILS" => 5,
                "MOVIES_CLIP_DETAILS" => 6,

                "SHOWS_FULLEPISODE_DETAILS" => 7,
                "SHOWS_CLIP_DETAILS" => 8,
            ),
            "staticPages" => array( 
                                    "/contact-us" => array(
                                                            "templateNameAndKey" => "STATIC_CONTACT_US",
                                                            "pageType" => 9
                                        ),
                                    "/privacy-policy" => array(
                                                            "templateNameAndKey" => "STATIC_PRIVACY_POLICY",
                                                            "pageType" => 9
                                    ),
                                    "/faq" => array(
                                                            "templateNameAndKey" => "STATIC_FAQ",
                                                            "pageType" => 9
                                    ),
                                    "/apphelp" => array(
                                                            "templateNameAndKey" => "STATIC_FAQ",
                                                            "pageType" => 9
                                    ),
                                    "/about-us" => array(
                                                            "templateNameAndKey" => "STATIC_ABOUT_US",
                                                            "pageType" => 9
                                    ),
                                    "/terms-of-use"  => array(
                                                            "templateNameAndKey" => "STATIC_TERMS_OF_USE",
                                                            "pageType" => 9
                                    )
                                ),
            "dynamicPages" => array( 
                                    "/myvoot" => array(
                                                            "templateNameAndKey" => "DYNAMIC_MYVOOT",
                                                            "pageType" => 10
                                        ),
                                    "/kids" => array(
                                                            "templateNameAndKey" => "DYNAMIC_KIDS",
                                                            "pageType" => 10
                                    ),
                                    "/kids/search" => array(
                                                            "templateNameAndKey" => "DYNAMIC_KIDS_SEARCH",
                                                            "pageType" => 10
                                    ),
                                    "/search" => array(
                                                            "templateNameAndKey" => "DYNAMIC_SEARCH",
                                                            "pageType" => 10
                                    ),
                                    "/shows" => array(
                                                            "templateNameAndKey" => "SHOWS_LIST",
                                                            "pageType" => 10
                                    ),
                                    "/movies" => array(
                                                            "templateNameAndKey" => "MOVIES_LIST",
                                                            "pageType" => 10
                                    ),
                                    "/vi" => array(
                                                            "templateNameAndKey" => "VOOT_INTEGRATION",
                                                            "pageType" => 10
                                    ),
                                    "/shows/voot-originals" => array(
                                                            "templateNameAndKey" => "VOOT_ORIGINALS",
                                                            "pageType" => 10
                                    ),
                                ),
                "routeTemplateChange" => array( 
                                    "/shows/bigg-boss" => "SHOWS_SLUG_SEASON_BIGG_BOSS",
                                    "/shows/bigg-boss-kannada" => "SHOWS_SLUG_SEASON_BB_KANADA",
                                    "/shows/naagin" => "SHOWS_SLUG_SEASON_NAAGIN",
                                    "/shows/splitsvilla" => "SHOWS_SLUG_SEASON_SPLITSVILLA",
                                ),
);
